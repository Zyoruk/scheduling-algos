# Scheduling Algorithms
## Authors
Luis
Dylan
## Description
Study of scheduling algorithms in performance, response times, processing which are going to be evaluated using a set of techniques.

## Algorithms
1. FIFO | FCFS
2. Round Robin
3. Short Job First - SJF
4. Short Remaining Time First

## Evaluation
### Criteria
1. CPU utilization
1. Response time
1. Performance

### Policies
1. Deterministic modeling
1. Queue models (statistics)
1. Simulations
1. Implementation

## Dependencies
- gcc

## Setup
Run the following commands
1. `cd scheduling-algos`
1. `./build` or `sh build.sh`  

After running these, you should find the compiled files at `./bin` 

## Running the algorithms
To run a compile file run `./bin/<name of file>`
