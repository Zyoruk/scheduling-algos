# !/bin/bash
LIB=../bin
cd src
for f in *.c ; 
    do echo "Compiling $f..."; 
    gcc -o "$LIB/${f%.c}" "$f" -pthread;
done
echo "Done"
