#include <stdio.h>
#include <pthread.h>
#include "./data.h"
#include "./process.h"

void display(POS *list);
void scheduling(POS *list);
void waitTime(POS *list);
void turnAroundTime(POS *list);
void* getAverageTimeRoutineFCFS(void *items);

//int main()
int main(int argumentsCount, char **argumentsValues)
{
    POS *items = getMockData(argumentsCount, argumentsValues);
    pthread_t execution;
    int iret = pthread_create(&execution, NULL, getAverageTimeRoutineFCFS, items);
    pthread_join(execution, NULL);
    return 0;
} 

void* getAverageTimeRoutineFCFS(void *arg) { 
    POS* items = (POS*) arg;
    scheduling(items);
    waitTime(items);
    turnAroundTime(items);
    display(items);
}

void display(POS *list)
{
    int i;
	int TotalWatingTime=0,TotalturnAroundTime=0;
    printf("\n\n\t\t\tOutput according to LRTF\n");
    printf("\n\t\t\t|===============================================================|");
    printf("\n\t\t\t|ArrivalTime\tburstime\twaitTime\tturnAroundTime  |");
    printf("\n\t\t\t|===============================================================|");    
    for (i = 0; i < MAX_SIZE; i++)
    {
        printf("\n\t\t\t%d,\t\t%d,\t\t%d,\t\t%d,\t\t", list[i].arrivalTime, list[i].burstime,list[i].waitTime,list[i].turnAroundTime);
        // printf("\a\n\t\t\t|---------------------------------------------------------------|");
		TotalWatingTime= TotalWatingTime+list[i].waitTime;
		TotalturnAroundTime= TotalturnAroundTime+list[i].turnAroundTime;
	} 
	printf("\n\n\t\t\tTotal Waiting Time is: = %d",TotalWatingTime);
	printf("\n\t\t\tTotal Turn around Time is: = %d\n\n",TotalturnAroundTime);
	printf("\n\n\t\t\tAverage Waiting Time is: = %d",TotalWatingTime/MAX_SIZE);
	printf("\n\t\t\tAverage Turn around Time is: = %d\n\n",TotalturnAroundTime/MAX_SIZE);
}

void scheduling(POS *list)
{
    int i, j;
    POS temp;
    
    for (i = 0; i < MAX_SIZE - 1; i++)
    {
        for (j = 0; j < (MAX_SIZE - 1-i); j++)
        {
            if (list[j].burstime > list[j + 1].burstime)
            {
                temp = list[j];
                list[j] = list[j + 1];
                list[j + 1] = temp;
            } 
            else if(list[j].burstime == list[j + 1].burstime)
            {
            	if(list[j].arrivalTime > list[j + 1].arrivalTime)
            	{
            	temp = list[j];
                list[j] = list[j + 1];
                list[j + 1] = temp;
                }
			}
        }     
    }
}


void waitTime(POS *list)
{
	int j,total;
    list[0].waitTime=0;
    for(j=1;j<MAX_SIZE;j++)
    {
        list[j].waitTime=list[j-1].waitTime+list[j-1].burstime;
        
    }
}


void turnAroundTime(POS *list)
{
	int j,total;
    
    for(j=0;j<MAX_SIZE;j++)
    {
        list[j].turnAroundTime=list[j].waitTime+list[j].burstime;
    }
}