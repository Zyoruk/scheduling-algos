#ifndef ARGSUTILS_H
#define ARGSUTILS_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Parses the script arguments
 * 
 * @param argumentsCount unsigned short 
 * @param argumentsValues char*
 * @return int* [executionTime,executionTimeType]
 */
int *parseParams(unsigned short argumentsCount, char *argumentsValues[])
{
    unsigned short realArgumentsCount;
    unsigned char wereArgumentsPassed, isCountPair;
    static int returnValues[3];
    char *executionTimeFlag = "-et",
         *executionTimeTypeFlag = "-ett",
         *sameArrivalTimeFlag = "-sat";

    realArgumentsCount = argumentsCount - 1;
    wereArgumentsPassed = realArgumentsCount > 0;
    isCountPair = realArgumentsCount % 2 == 0;

    if (wereArgumentsPassed && isCountPair)
    {
        // Let's check that the flags exist
        for (unsigned int i = 1; i <= realArgumentsCount; i += 2)
        {
            const char *value = argumentsValues[i];
            if (strcmp(value, executionTimeFlag) == 0 && strcmp(value, executionTimeTypeFlag) == 0 && strcmp(value, sameArrivalTimeFlag) == 0)
            {
                return NULL;
            }

            unsigned short argumentValue = atoi(argumentsValues[i + 1]);
            if (strcmp(value, executionTimeFlag) == 0)
            {
                returnValues[0] = argumentValue;
            }
            if (strcmp(value, executionTimeTypeFlag) == 0)
            {
                returnValues[1] = argumentValue;
            }
            if (strcmp(value, sameArrivalTimeFlag) == 0)
            {
                returnValues[2] = argumentValue;
            }
        }
    }
    return returnValues;
}
#endif