#ifndef PROCESS_H
#define PROCESS_H
/**
 * Represent Process Data meant to use during the simulations
 */
typedef struct ProcessOS
{
    unsigned short arrivalTime;
    unsigned short burstime;
    unsigned short waitTime;
    unsigned short turnAroundTime;
} POS;
#endif
