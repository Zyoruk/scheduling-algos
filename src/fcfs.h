// FCFS
#ifndef FCFS_H
#define FCFS_H
#include <stdio.h>
#include <pthread.h>
#include "./process.h"
#include "./data.h"
#include "./utils.h"

/** 
 * Generate waitTime and turnAround time
 * @param POS* items - pointer to array of Processes.
*/
void* generateTimesFCFS(POS *items)
{
   // waiting time for first process is 0
   items[0].waitTime = 0;

   for (int i = 1; i < MAX_SIZE; i++)
   {
      items[i].waitTime = items[i - 1].burstime + items[i - 1].waitTime;
   }
   for (int i = 0; i < MAX_SIZE; i++)
   {
      items[i].turnAroundTime = items[i].burstime + items[i].waitTime;
   }
}

void *getAverageTimeRoutineFCFS(void *itemsPtr)
{
   POS *items = (POS *)itemsPtr;
   getAverageTime(items, generateTimesFCFS);
}
#endif