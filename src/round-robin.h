#ifndef RR_H
#define RR_H

#include <stdio.h>
#include <pthread.h>
#include "./process.h"
#include "./data.h"
#include "./utils.h"
#include "./args.utils.h"

#define QUANTUM 20

// Find the all the processes' waiting time
void findWaitingTimeRR(POS *items)
{
   int remainingTime[MAX_SIZE];
   for (int i = 0; i < MAX_SIZE; i++)
      remainingTime[i] = items[i].burstime;

   int currentTime = 0;

   // Roundrobin until we're done
   while (1)
   {
      unsigned int done = 1;

      // Traverse all processes one by one repeatedly
      for (int i = 0; i < MAX_SIZE; i++)
      {
         if (remainingTime[i] > 0)
         {
            done = 0;

            if (remainingTime[i] > QUANTUM)
            {
               currentTime += QUANTUM;
               remainingTime[i] -= QUANTUM;
            }
            else
            {
               currentTime = currentTime + remainingTime[i];
               items[i].waitTime = currentTime - items[i].burstime;
               remainingTime[i] = 0;
            }
         }
      }

      // If all processes are done
      if (done == 1)
         break;
   }
}

// Function to calculate turn around time
void findTurnAroundTimeRR(POS *items)
{
   for (int i = 0; i < MAX_SIZE; i++)
      items[i].turnAroundTime = items[i].burstime + items[i].waitTime;
}

void *generateTimes(POS *items)
{
   findWaitingTimeRR(items);
   findTurnAroundTimeRR(items);
}

void *getAverageTimeRoutineRR(void *itemsPtr)
{
   POS *items = (POS *)itemsPtr;
   getAverageTime(items, generateTimes);
}

#endif