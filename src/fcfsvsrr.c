// FCFS
#include <stdio.h>
#include <pthread.h>
#include "./process.h"
#include "./data.h"
#include "./utils.h"

#include "./round-robin.h"
#include "./fcfs.h"

void *getAverageTimeRoutine(void *itemsPtr)
{
    getAverageTimeRoutineRR(itemsPtr);
    getAverageTimeRoutineFCFS(itemsPtr);
}

// FCFS Run
int main(int argumentsCount, char *argumentsValues[])
{
    POS *items = getMockData(argumentsCount, argumentsValues);
    pthread_t execution;
    int iret = pthread_create(&execution, NULL, getAverageTimeRoutine, items);
    pthread_join(execution, NULL);
    return 0;
}
