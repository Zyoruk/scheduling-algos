#include "./round-robin.h"
#include "./process.h"
#include <pthread.h>

/** 
 * Driver code
 * -e => 
 * 1) Mismos tiempos de ejecución = X
 * 2) Mismo de llegada = X
 * 3) Ejecución Lineal Ascendente = 1 
 * 4) Ejecución Lineal Variantes = 2
 * Note: Execution Type higher prio than execution time
 * 
*/
int main(unsigned short argumentsCount, char *argumentsValues[])
{
   POS *items = getMockData(argumentsCount, argumentsValues);
   pthread_t execution;
   int iret = pthread_create(&execution, NULL, getAverageTimeRoutineRR, items);
   pthread_join(execution, NULL);
   return 0;
}
