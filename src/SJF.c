#include <stdio.h>
#include <pthread.h>
#include "./process.h"
#include "./data.h"

void* getAverageTimeRoutineFCFS(void *arg);

//int main()
int main(int argumentsCount, char **argumentsValues)
{
    POS *items = getMockData(argumentsCount, argumentsValues);
    pthread_t execution;
    int iret = pthread_create(&execution, NULL, getAverageTimeRoutineFCFS, items);
    pthread_join(execution, NULL);
    return 0;
} 

void* getAverageTimeRoutineFCFS(void *arg) { 
    POS* items = (POS*) arg;

    printf("SJF: SHORTEST JOB FIRST");
    // Algoritmo SJF
    // ordenamos de menor a mayor
    for (int i = 0; i < MAX_SIZE - 1; i++)
    {
        for (int j = i + 1; j < MAX_SIZE; j++)
        {
            if (items[j].burstime < items[i].burstime)
            {
                POS aux = items[j];
                items[j] = items[i];
                items[i] = aux;
            }
        }
    }
    double tf = 0, tp; // tiempo promedio.
    printf("\nProceso ,Burstime ,Waittime ,TurnAroundTime \n");
    for (int i = 0; i < MAX_SIZE; i++)
    {
        items[i].waitTime = tf;
        tf += items[i].burstime;
        tp = tp + tf;
        items[i].turnAroundTime = tf;
        printf("\n %d      ,%i        ,%i       ,%i", items[i].arrivalTime, items[i].burstime, items[i].waitTime, items[i].turnAroundTime);
    }
    printf("\n——————————-");
    printf("\nLa suma de los procesos %2.1f", tp);
    tp = tp / MAX_SIZE;
    printf("\n\nTiempo promedio en SJF fue de: %2.2f:", tp);
}