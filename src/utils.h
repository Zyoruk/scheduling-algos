#ifndef UTILS_H
#define UTILS_H
#include <stdio.h>
#include <pthread.h>
#include "./process.h"
#include "./data.h"

/** 
 * Function to calculate average time
 * @param POS* items - pointer to array of Processes.
 * @param void* generateTimes - pointer to function
 * @return void
*/
void getAverageTime(POS* items, void* generateTimes(POS*))
{
   int total_wt = 0, total_tat = 0;
   items[MAX_SIZE - 1].waitTime = 0;
   items[MAX_SIZE - 1].turnAroundTime = 0;

   (*generateTimes)(items);

   //Display processes along with all details
   printf("Arrival   Burst   Waiting   Turn around\n");

   // Calculate total waiting time and total turn
   // around time
   for (int i = 0; i < MAX_SIZE; i++)
   {
      total_wt = total_wt + items[i].waitTime;
      total_tat = total_tat + items[i].turnAroundTime;
      printf("   %i ,", (i + 1));
      printf("       %i ,", items[i].burstime);
      printf("       %i,", items[i].waitTime);
      printf("       %i,\n", items[i].turnAroundTime);
   }

   int averageWaitingTime = (float)total_wt / (float)MAX_SIZE - 1;
   int averageTurnAroundTime = (float)total_tat / (float)MAX_SIZE - 1;
   printf("Average waiting time = %d\n", averageWaitingTime);
   printf("Average turn around time = %d \n", averageTurnAroundTime);
}

#endif