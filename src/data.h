#ifndef DATA_H
#define DATA_H
#include <stdlib.h>
#include <stdio.h>
#include "process.h"
#include <time.h>
#include "./args.utils.h"

#define RAND_CONSTANT 60
#define MAX_SIZE 20

int getRand(unsigned short base)
{
    const int min = (base - 1) * RAND_CONSTANT;
    const int max = base * RAND_CONSTANT;
    return rand() % (max + 1 - min) + min;
}


int getSmallRand()
{
    const int min = 0;
    const int max =  4;
    return rand() % (max + 1 - min) + min;
}


/** 
 * Generates an array of mocked data
 * @return ProcessOS[populationSize]
*/
POS *getMockData(unsigned short argumentsCount, char *argumentsValues[])
{
    srand(time(NULL));

    const int *parsedParams = parseParams(argumentsCount, argumentsValues);
    int executionTime, executionTimeType, sameArrivalTime;

    if (parsedParams != NULL)
    {
        executionTime = *(parsedParams);
        executionTimeType = *(parsedParams + 1);
        sameArrivalTime = *(parsedParams + 2);
        printf("Running with arguments:\n");
        printf("-et : %d\n", executionTime);
        printf("-ett : %d\n", executionTimeType);
        printf("-sat : %d\n", sameArrivalTime);
    }

    POS *items;
    if ((items = (struct ProcessOS *)malloc(MAX_SIZE * sizeof(POS))) == NULL)
        puts("malloc fail");

    for (int n = 0; n < MAX_SIZE; n++)
    {
        if (sameArrivalTime > 0)
        {
            items[n].arrivalTime = getSmallRand();
        }
        else
        {
            items[n].arrivalTime = n;
        }

        if (executionTime > 0 && executionTimeType == 0)
        {
            items[n].burstime = executionTime;
        }
        else if (executionTime == 0 && executionTimeType == 1)
        {
            items[n].burstime = getRand(n + 1);
        }
        else if (executionTime == 0 && executionTimeType == 2)
        {
            items[n].burstime = getRand(1);
        }
        else
        {
            items[n].burstime = getRand(1);
        }

        items[n].waitTime = n;
        items[n].turnAroundTime = n;
    }
    return items;
}
#endif
